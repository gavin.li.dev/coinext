import { ObjectId } from "mongodb";
import { z } from "zod";

export const TransactionSchema = z.object({
    walletId: z.instanceof(ObjectId),
    coinId: z.string().min(3).max(12),
    direction: z.enum(['BUY', 'SELL']),
    amount: z.number(),
    price: z.number(),
    cost: z.number(),
    createdAt: z.date()
}).strict();

