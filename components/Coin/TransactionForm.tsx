import { useState, ChangeEvent, MouseEvent } from 'react';
import { Box, Button, FormControl, Input, InputAdornment, InputLabel } from "@mui/material";

export default function TransactionForm({ coin, submit }: {coin:any, submit:Function}) {

    const [amount, setAmount] = useState(0);
    const [unitPri, setUnitPri] = useState(coin.current_price);
    const [cost, setCost] = useState(0);

    function updateAmount(e:ChangeEvent<any>) {
        // const val = parseFloat(e.target.value);
        const val = e.target.value;
        console.log(val);
        if (val < 0 || isNaN(val)) {
            return;
        }
        setAmount(val);

        const newCost = +(val * unitPri).toFixed(2);
        setCost(newCost);
    }

    function updateUnitPri(e:ChangeEvent<any>) {
        const val = parseFloat(e.target.value);
        if (val < 0) {
            return;
        }
        setUnitPri(val);

        const newCost = +(amount * val).toFixed(2);
        setCost(newCost);
    }

    function updateCost(e:ChangeEvent<any>) {
        const val = parseFloat(e.target.value);
        if (val < 0) {
            return;
        }
        setCost(val);

        if (amount > 0) {
            const newUnitPri = +(val / amount).toFixed(2);
            setUnitPri(newUnitPri);
        }
    }

    const submitForm = (e:MouseEvent) => {
        if(amount <= 0) {
            e.preventDefault();
            return;
            /**
             * @todo set error message to form
             */
        }
        submit(amount, unitPri, cost);
    }

    const buttonColor = 'primary';

    return (
        <form>
            <Box sx={{display:'flex',gap:4,padding:4}}>
                <FormControl size="small">
                    <InputLabel htmlFor="amount">Amount</InputLabel>
                    <Input
                        id="amount"
                        size="small"
                        value={amount}
                        onChange={(e) => updateAmount(e)}
                    />
                </FormControl>
                <FormControl>
                    <InputLabel htmlFor="unit-price">Unit Price</InputLabel>
                    <Input
                        id="unit-price"
                        size="small"
                        startAdornment={<InputAdornment position="start">$</InputAdornment>}
                        value={unitPri}
                        onChange={(e) => updateUnitPri(e)}
                    />
                </FormControl>
                <FormControl>
                    <InputLabel htmlFor="cost">Cost</InputLabel>
                    <Input
                        id="cost"
                        size="small"
                        placeholder='0'
                        startAdornment={<InputAdornment position="start">$</InputAdornment>}
                        value={cost}
                        onChange={(e) => updateCost(e)}
                    />
                </FormControl>
                <Button
                    variant="contained"
                    color={buttonColor}
                    onClick={(e) => submitForm(e)}
                >Buy</Button>
            </Box>
        </form>
    )
}