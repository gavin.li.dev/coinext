import { Avatar, Box, Typography } from '@mui/material';
import Grid from '@mui/material/Unstable_Grid2';

export default function CoinAsset({coin, position}: {coin:any, position: any}) {
    const [priceColor, pricePre] = coin.price_change_24h > 0 ? ["green",'+'] : ["red",""];

    return (
        <Grid container sx={{bgcolor:'primary.light',borderRadius:1,padding:2}}>
            <Grid xs={1} display="flex" alignItems="center">
                <Avatar src={coin.image}></Avatar>
            </Grid>
            <Grid xs={1} display="flex" flexDirection="column" justifyContent="center">
                <Typography variant="h6" m="0">
                    ${coin.current_price}
                </Typography>
                <Typography color={priceColor} variant='caption'>
                    $ {pricePre}{coin.price_change_24h.toFixed(2)} 
                </Typography>
            </Grid>
            <Grid xs={3} display="flex" justifyContent="center">
                <Box>
                    <Typography color="#666" variant='caption'>
                        Asset Holding
                    </Typography>
                    <Typography color="secondary" fontSize="larger">
                        {position.amount.toFixed(4)}
                    </Typography>
                </Box>
            </Grid>
            <Grid xs={3} display="flex" justifyContent="center">
                <Box>
                    <Typography color="#666" variant='caption'>
                        Asset Cost
                    </Typography>
                    <Typography color="secondary" fontSize="larger">
                        $ {position.cost.toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2})}
                    </Typography>
                </Box>
            </Grid>
        </Grid>
    )
}