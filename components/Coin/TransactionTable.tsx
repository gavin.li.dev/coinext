import { Box, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";

type Transaction = {
    id: string
    amount: number
    unitPrice: number
    cost: number
    createdAt: Date
}

export default function TransactionTable({ transactions }: {transactions: Transaction[]}) {

    return (
        transactions.length > 0 ?
        <TableContainer>
            <Table size="small">
                <TableHead>
                    <TableRow>
                        <TableCell>Amount</TableCell>
                        <TableCell align="right">Unit Price</TableCell>
                        <TableCell align="right">Cost</TableCell>
                        <TableCell align="right">Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    { transactions.map((t, idx) => (
                    <TableRow key={t.id} sx={idx%2==0?{backgroundColor:'#f8f8f8'}:{}}>
                        <TableCell>{t.amount}</TableCell>
                        <TableCell align="right">${t.unitPrice.toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2})}</TableCell>
                        <TableCell align="right">${t.cost.toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2})}</TableCell>
                        <TableCell align="right">{new Date(t.createdAt).toLocaleString()}</TableCell>
                    </TableRow>
                    )) }
                </TableBody>
            </Table>
        </TableContainer>:
        <Box sx={{width:"100%",minHeight:"200px",display:"flex",justifyContent:"center",alignItems:"center"}} bgcolor="secondary.light">
            <Typography variant="h3" color="white">
                No Transaction
            </Typography>
        </Box>
    )
}