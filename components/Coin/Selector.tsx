import { Avatar, Box, List, ListItem, ListItemButton, ListItemAvatar, ListItemText, Grid, Typography } from '@mui/material';
import { CoinItem } from '@prisma/client';

export default function CoinSelector({coins, onSelect}: {coins:CoinItem[], onSelect: Function}) {

    return (
        <Box>
            <List dense={true}>
                { coins.map((coin:CoinItem) => (
                <ListItem key={coin.id} onClick={()=>onSelect(coin.id)} sx={{borderBottom:'1px solid #eee', padding:0}}>
                    <ListItemButton>
                        <ListItemAvatar sx={{minWidth:32}}>
                            <Avatar sx={{width:24,height:24}} alt={coin.name} src={coin.image} />
                        </ListItemAvatar>
                        <ListItemText primary={coin.name} />
                    </ListItemButton>
                </ListItem>
                )) }
            </List>
        </Box>
    )
}