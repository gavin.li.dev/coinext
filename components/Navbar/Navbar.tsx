import Route from 'next/router';

import { Divider, Drawer, List, ListItem, Toolbar }from '@mui/material';
import { ListItemButton, ListItemIcon, ListItemText} from '@mui/material';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';

import { useGuard } from 'components/RouteGuard';
import { NavbarItemlist } from './const/ItemList';

const Navbar = () => {
    const { user, setUser } = useGuard();
    const drawerWidth = 240;
    // const navigate = useNavigate();

    const logout = function() {
        localStorage.removeItem('u');
        localStorage.removeItem('credential');

        setUser({ isAuthorized: false });
        Route.push('/login');
    }

    return (
        <Drawer
            sx={{
            width: drawerWidth,
            flexShrink: 0,
            '& .MuiDrawer-paper': {
                width: drawerWidth,
                boxSizing: 'border-box',
            },
            }}
        variant="permanent" anchor="left" >
            <Toolbar />
            <Divider />
            <List>
                { NavbarItemlist.map((item, index) => (
                <ListItem key={index} onClick={() => {Route.push(item.route)}} disablePadding>
                    <ListItemButton>
                        <ListItemIcon>{item.icon}</ListItemIcon>
                        <ListItemText primary={item.label} />
                    </ListItemButton>
                </ListItem>
                )) }
            </List>
            <Divider />
            <List>
                <ListItem onClick={logout} disablePadding>
                    <ListItemButton>
                        <ListItemIcon><ExitToAppIcon /></ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItemButton>
                </ListItem>
            </List>
        </Drawer>
    )
}

export default Navbar