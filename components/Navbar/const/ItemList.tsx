import PeopleIcon from '@mui/icons-material/People';
import CurrencyBitcoinIcon from '@mui/icons-material/CurrencyBitcoin';

export const NavbarItemlist = [
    {
        id: 0,
        icon: <CurrencyBitcoinIcon />,
        label: 'Overview',
        route: '/'
    }
]