import { Box, Chip, Typography } from '@mui/material';
import JavascriptIcon from '@mui/icons-material/Javascript';

import AccountMenu from '../AccountMenu/AccountMenu';

export default function Header() {
    const headerStyle = {
        wrapper: {
            width: '100%',
            display: 'flex',
            flexDirection: 'row',
            backgroundColor: '#f8f8f8'
        }
    }

    return (
        <Box sx={headerStyle.wrapper}>
            <Box sx={{flexGrow:1}} display="flex" alignItems="center">
                <Box pl={4} pr={4}>
                    <Chip icon={<JavascriptIcon />}
                        label="Gitlab"
                        variant="outlined"
                        component="a"
                        href="https://gitlab.com/gavin.li.dev/coinext" target="_blank"
                        clickable
                    />
                </Box>
                <Typography variant="overline">Data could be removed anytime without prior notice</Typography>
            </Box>
            <AccountMenu></AccountMenu>
        </Box>
    )
}