import { useRouter } from 'next/router';
import { createContext, ReactElement, useContext, useEffect, useState } from 'react';

type User = {
    isAuthorized: boolean,
    name?: string,
    pic?: string
}

type AuthContextType = {
    user: User
    setUser: (u: User) => void
}

const AuthContext = createContext<AuthContextType>({
    user: {
        isAuthorized: false
    },
    setUser: () => {}
});

export default function RouteGuard({ children }:{children: ReactElement}) {
    const router = useRouter();
    const [user, setUser] = useState<User>({isAuthorized: false});
    
    let allowed = false;

    useEffect(() => {
        authCheck(router.asPath);
    }, []);

    console.log(allowed, user.isAuthorized);

    const authCheck = (url:string) => {
        if(url === '/login' || url === '/register') {
            allowed = true;
            return;
        }

        console.log(url);

        const u = localStorage.getItem('u');
        if(!u) {
            router.push('/login');
            return;
        }

        const uObj = JSON.parse(u);
        if(uObj && uObj.name) {
            setUser({
                isAuthorized: true,
                name: uObj.name,
                pic: uObj.pic
            });

            return;
        } else {
            router.push('/login');
            return;
        }
    }

    return (
        <AuthContext.Provider value={{user,setUser}}>
            {(allowed || user.isAuthorized) && children}
        </AuthContext.Provider>
    )
}

export function useGuard() {
    return useContext(AuthContext);
}