import { Box, Typography } from "@mui/material";
import styles from './Performance.module.css';

const local = (num:number) => {
    return num.toLocaleString('en-US', {minimumFractionDigits:2, maximumFractionDigits:2})
}

export default function Performance({cost=0,valuation}:{cost:number,valuation:number}) {
    const change = valuation - cost;
    return (
        <Box p={4} m={4} border="1px solid #f0f0f0" borderRadius={4}>
            <Typography variant="caption">Porfolio Valuation</Typography>
            <Box sx={{display:'flex',justifyContent:'start',alignItems:'center'}}>
                <Typography className={styles.colorText} fontSize="2.4rem">
                    $ {local(valuation)}
                </Typography>
                <Box className={[change >= 0 ? styles.up:styles.down, styles.margin].join(' ')}>{local(change)}</Box>
            </Box>
            <Typography variant="subtitle1">${local(cost)}</Typography>
        </Box>
    )
}