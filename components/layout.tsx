import { FC, ReactElement } from 'react';
import Box from '@mui/material/Box';

import Navbar from './Navbar/Navbar';
import Header from './Header/Header';

export default function Layout({ children }: { children: ReactElement } ) {
    return (
        <Box sx={{display: 'flex'}}>
            <Navbar />
            <Box sx={{ flexGrow: 1, bgcolor: 'background.default' }}>
                <Header />
                { children }
            </Box>
        </Box>
    )
}