import { useState, MouseEvent } from 'react'
import Link from 'next/link';

import { Avatar, Box, IconButton, ListItemIcon, Menu, MenuItem } from '@mui/material';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

import { useGuard } from 'components/RouteGuard';

export default function AccountMenu() {
    const { user } = useGuard();
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement>();

    let open = Boolean(anchorEl);

    const handleClick = (e:MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(e.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(undefined);
    }

    // const userPic: string = user.pic;
    return (
        <Box sx={{ p:1 }}>
            <IconButton
                onClick={handleClick}
                size="small"
                aria-haspopup="true"
            >
                <Avatar alt={user.name} src={user.pic} style={{
                    border: '1px solid lightgray',
                    width: 24,
                    height: 24
                }}></Avatar>
            </IconButton>

            <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                    elevation: 0,
                    sx: {
                            overflow: 'visible',
                            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                            mt: 1.5,
                            '& .MuiAvatar-root': {
                            width: 32,
                            height: 32,
                            ml: -0.5,
                            mr: 1,
                        },
                        '&:before': {
                            content: '""',
                            display: 'block',
                            position: 'absolute',
                            top: 0,
                            right: 4,
                            width: 10,
                            height: 10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(-50%) rotate(45deg)',
                            zIndex: 0,
                        },
                    },
                }}
                transformOrigin={{ horizontal: 'right', vertical: 'top' }}
                anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
            >
                <MenuItem>
                    <ListItemIcon><AccountCircleIcon /></ListItemIcon>
                    <Link href="/profile/">
                        Profile
                    </Link>
                </MenuItem>
            </Menu>
        </Box>
    )
}
