import ApplicationError from "./application.error";

export default class AuthError extends ApplicationError {
    static AUTH_HEADER_REQUIRED = 'auth_header_required';
    static AUTH_FORMAT_ERROR = 'auth_format_error';
    static AUTH_TOKEN_INVALID = 'auth_token_invalid';
    static USER_NOT_FOUND = 'user_not_found';
    
    constructor(msg: string, code: string) {
        super(msg);
        this.code = code;
    }
}