export default class ApplicationError extends Error {

    private _code: string = 'application_error';

    get code() {
        return this._code;
    }

    set code(code) {
        this._code = code;
    }
}