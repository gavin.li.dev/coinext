# coinext

The purpose of this App <Coin Tracker> is to demostrate a oauth login using google as a identity provider.

Learning through building

## Getting started

Go go yoroshiku.click for a live Demo

## install locally

Clone the project to local environment with nodejs version > 16

A Mongodb database is also required, or use a remote service from Atlas

Add a .env file to the root folder, the file should contain a single param DATABASE_URL

eg: 
DATABASE_URL="mongodb+srv://<dbuser>:<dbpass>@<dbhost>/<dbname>?retryWrites=true&w=majority"

Use "npm install" to fetch libs

Execute "npm run db:gen" to create Prisma data adaptor

Execute "npm run dev" to serve the app locally on localhost:3000
