import { MongoClient } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import { RequestCtx, apiContext } from "./api.wrapper";

export default async function dbMw(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {
    const dbURL = process.env.DATABASE_URL;
    if(!dbURL) {
        throw new Error('Must have a database connection String');
    }
    const client = await MongoClient.connect(dbURL);
    const coinDb = client.db('coin');

    ctx.dbClient = client;
    ctx.coinDb = coinDb;

    apiContext.dbClient = client;
    apiContext.coinDb = coinDb;
}