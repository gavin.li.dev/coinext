import { NextApiRequest, NextApiResponse } from "next";
import { ObjectId } from 'mongodb';
import * as jose from "jose";

import { RequestCtx, apiContext } from "./api.wrapper";
import AuthError from "errors/auth.error";

export default async function jwtMw(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {

    const coinDb = ctx.coinDb;

    if(!coinDb) {
        throw new Error('Must have a database connection');
    }

    const authHeader = req.headers.authorization;

    if (!authHeader || authHeader == "") {
        throw new AuthError('auth header not found', AuthError.AUTH_HEADER_REQUIRED);
    }

    const bearer = authHeader.split(' ');
    if (bearer.length != 2 || bearer[0].toLowerCase() != 'bearer') {
        throw new AuthError('auth header format error', AuthError.AUTH_FORMAT_ERROR);
    }

    const token = bearer[1];

    /**
     * @todo load secretKey from DB
     */
    var secretKey = new Uint8Array([0x62, 0x69, 0x74, 0x63, 0x6f, 0x69, 0x6e]);
    let userId;
    try {
        const { payload, protectedHeader } = await jose.jwtVerify(token, secretKey);
        userId = payload.sub;
    } catch (err) {
        throw new AuthError('auth token not valid', AuthError.AUTH_TOKEN_INVALID);
    }

    const user = await coinDb.collection('user').findOne({
        _id: new ObjectId(userId)
    });
    if(!user) {
        throw new AuthError('auth user not found', AuthError.USER_NOT_FOUND);
    }

    ctx.user = user;
    ctx.userId = user._id;

    apiContext.user = user;
    apiContext.userId = user._id;
}