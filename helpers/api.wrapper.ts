import { Db, Document, MongoClient, ObjectId } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import jwtMw from "./jwt.mw";
import dbMw from './db.mw';
import AuthError from "errors/auth.error";

export type RequestCtx = {
    user?: Document,
    userId?: ObjectId,
    coinDb?: Db,
    dbClient?: MongoClient
}

type FTHandler = (request: NextApiRequest, response: NextApiResponse, context: RequestCtx) => Promise<void>

type T_ApiHandlers = {
    get?: FTHandler,
    post?: FTHandler,
    put?: FTHandler,
    delete?:  FTHandler
}

class ApiContext {
    protected _user!: Document;
    protected _userId!: ObjectId;
    protected _coinDb!: Db;
    protected _dbClient!: MongoClient;

    set user(val: Document) {
        this._user = val;
    }

    get user(): Document {
        if(!this._user) {
            throw new Error('must init user in middleware');
        }
        return this._user;
    }

    set userId(val: ObjectId) {
        this._userId = val;
    }

    get userId(): ObjectId {
        if(!this._userId) {
            throw new Error('must init user in middleware');
        }
        return this._userId;
    }

    set coinDb(val: Db) {
        this._coinDb = val;
    }

    get coinDb(): Db {
        if(!this._coinDb) {
            throw new Error('must init user in middleware');
        }
        return this._coinDb;
    }

    set dbClient(val: MongoClient) {
        this._dbClient = val;
    }

    get dbClient(): MongoClient {
        if(!this._dbClient) {
            throw new Error('must init user in middleware');
        }
        return this._dbClient;
    }
}

export const apiContext = new ApiContext();

export function apiWrapper(handlers: T_ApiHandlers, requiredMws = ['db', 'jwt']) {
    let ctx:RequestCtx = {};

    return async (req: NextApiRequest, res: NextApiResponse) => {
        const method = req.method?.toLowerCase();
        if(!method) {
            throw new Error('System error, no request method');
        }
        if(!handlers.hasOwnProperty(method))
            return res.status(400).end(`Method ${method} Not Allowed`);

        try {
            if(requiredMws.includes('db')) {
                await dbMw(req, res, ctx);
            }
            if(requiredMws.includes('jwt')) {
                await jwtMw(req, res, ctx);
            }
        } catch(err) {
            if(err instanceof AuthError) {
                return res.status(419).end('login required');
            }
            
            console.error(err);
            return res.status(500).end('unexpected error');
        }

        //try controller errors
        try {
            switch(method) {
                case 'get':
                    if(handlers.get)
                        await handlers.get(req, res, ctx);
                    break;
                case 'post':
                    if(handlers.post) 
                        await handlers.post(req, res, ctx);
                    break;
                case 'put':
                    if(handlers.put)
                        await handlers.put(req, res, ctx);
                    break;
                case 'delete':
                    if(handlers.delete)
                        await handlers.delete(req, res, ctx);
                    break;
                default:
                    return res.status(400).end(`Method ${method} Not Allowed`);
            }
        } catch(err) {
            console.error(err);
            return res.status(500).end('unexpected error');
        }
    }
}