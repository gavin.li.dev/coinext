import { useRouter } from 'next/router';
import { useState } from 'react';

import { Box, Button, Paper, Typography } from '@mui/material';
import DataSaverOnIcon from '@mui/icons-material/DataSaverOn';

import { useTransactionQuery, useTransactionMutation } from 'hooks/transaction.hook';
import { useCoinQuery } from 'hooks/coin.hook';

import CoinAsset from 'components/Coin/Asset';
import TransactionForm from 'components/Coin/TransactionForm';
import TransactionTable from 'components/Coin/TransactionTable';
import { useEffect } from 'react';

export default function Asset() {
    const router = useRouter();
    const coinId = router.query.id;
    
    if(Array.isArray( coinId )) {
        throw new Error('Router must have an ID');
    }

    const [position, setPosition] = useState({ amount: 0, cost: 0 });
    const [hideForm, setHideForm] = useState(true);
    
    // init mutation[post] function
    const mutation = useTransactionMutation();
    
    // api calls
    const { data: coinDataArr, isLoading: coinIsLoading } = useCoinQuery(coinId);
    const { data: transactionData, isLoading: transactionIsLoading } = useTransactionQuery(coinId);

    let assetPosition;
    if(transactionData) {
        const assetKey = coinId;
        if(assetKey) {
            assetPosition = transactionData.wallet?.assets[assetKey] || {amount:0,cost:0};
        } else {
            assetPosition = {amount:0,cost:0};
        }
    }

    // event functions
    const onTranClick = () => {
        setHideForm(false);
    }

    const onTranFormSubmit = (amount:number, unitPrice:number, cost:number) => {
        const nt = {
            coinId,
            amount,
            price: unitPrice,
            cost,
            createdAt: new Date()
        };

        const newPosition = { ...position, amount: position.amount + Number(amount), cost: position.cost + Number(cost) };

        setPosition(newPosition);
        setHideForm(true);

        mutation.mutate(nt);
    }

    let coinData;
    if(!coinIsLoading) {
        coinData = coinDataArr[0];
    }

    return (
        <Paper style={{ margin: "16px 0px", padding: "16px" }}>
            {coinIsLoading || transactionIsLoading ?
                <Typography variant='h6' mb='4'>Loading Market Price ...</Typography> :
                <>
                    <Typography variant='h6' mb='4'>{coinData.name} Position</Typography>
                    <CoinAsset coin={coinData} position={assetPosition}></CoinAsset>

                    <Box mt={4} p={1} bgcolor="#f0f0f0" borderRadius={1}>
                        <Box sx={{ border: "1px solid white" }} p={1}>
                            {hideForm ?
                                <Button onClick={onTranClick}
                                    startIcon={<DataSaverOnIcon />}
                                    variant="contained"
                                    color="primary"
                                >Add Transaction</Button> :
                                <TransactionForm coin={coinData} submit={onTranFormSubmit} />
                            }
                        </Box>
                    </Box>
                </>
            }

            <Box mt={4}>
                {transactionIsLoading ?
                    <Box>Loading Transactions ...</Box> :
                    <TransactionTable transactions={transactionData.transactions}></TransactionTable>
                }
            </Box>
        </Paper>
    )
}
