import Link from 'next/link';
import { ReactElement } from 'react';
import Paper from "@mui/material/Paper";

export default function Register() {
    return (
        <h1>Register Form</h1>
    )
}

Register.getLayout = function getLayout(page: ReactElement) {
    console.log('get layout function in register form function called');
    return (
        <Paper elevation={3}>
            {page}
            <Link href="/login">
                <a>Login</a>
            </Link>
            <Link href="/">
                <a>HOME</a>
            </Link>
        </Paper>
    )
}