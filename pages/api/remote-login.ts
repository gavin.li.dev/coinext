import { NextApiRequest, NextApiResponse } from 'next';
import { Db } from 'mongodb';
// import { auth_pub_key, PrismaClient } from "@prisma/client";
import axios from "axios";
import * as jose from 'jose';
import { z } from 'zod';

import { apiWrapper, RequestCtx, apiContext } from "helpers/api.wrapper";

const wrappedApiHandlers = apiWrapper({
    post: apiPostHandler
}, ['db']);
export default wrappedApiHandlers;

const GOOGLE_WELL_KNOWN_URI = "https://accounts.google.com/.well-known/openid-configuration";

// const prisma = new PrismaClient({
//     log: ['warn', 'error']
// });

const GPayloadSchema = z.object({
    sub: z.string().min(1),
    name: z.string().min(1),
    picture: z.string().optional()
})

const GPubeySchema = z.object({
    kid: z.string().min(1),
    kty: z.string().min(1),
    e: z.string().min(1),
    n: z.string().min(1),
    alg: z.string().min(1),
});

// const savePublicKey = async (keyData:any):Promise<auth_pub_key> => {
//     try {
//         const keyModel = await prisma.auth_pub_key.create({data: keyData});
//         return keyModel;
//     } catch(err) {
//         console.error(err);
//         throw err;
//     }
// }

const getPublicKey = async (kid: string, coinDb:Db) => {
    let keyModel = await coinDb.collection('auth_pub_key').findOne({
        where: {kid}
    })

    // key not found in db
    if(!keyModel) {
        const result = await axios.get(GOOGLE_WELL_KNOWN_URI);
        const jwksUri = result.data.jwks_uri;

        if(!jwksUri) {
            throw new Error('google well known endpoint err');
        }

        const pubkeyResults = await axios.get(jwksUri);
        const pubkeys: Array<any> = pubkeyResults.data.keys;

        const pubkey = pubkeys.find((k) => k.kid === kid);

        if (!pubkey) {
            throw new Error('public key not found from Google!');
        }

        let parsedPubkey;
        try {
            parsedPubkey = GPubeySchema.parse(pubkey);
        } catch(err) {
            console.error(err);
            throw new Error('Google public key format error!');
        }

        const keyData = {
            kid: parsedPubkey.kid,
            key: {
                kty: parsedPubkey.kty,
                e: parsedPubkey.e,
                n: parsedPubkey.n,
                alg: parsedPubkey.alg
            },
            iss: "google",
            ver: "v1"
        }

        try {
            await coinDb.collection('auth_pub_key').insertOne(keyData);
            return keyData;
        } catch(err) {
            console.error(err);
            throw err;
        }
    }

    return keyModel;
}

async function apiPostHandler(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {

    // try {
    //     await prisma.$connect();
    // } catch(err) {
    //     console.error(err);
    //     return res.status(500).send('Error connect DB');
    // }


    const coinDb = apiContext.coinDb;
    
    const cre = req.body['credential'];
    if(!cre) {
        return res.status(400).send('credential not set');
    }

    let kid;
    try {
        const protectedHeader = jose.decodeProtectedHeader(cre);
        kid = protectedHeader.kid;
        if(!kid) {
            throw new Error('kid not exist in token')
        }
    } catch(err) {
        console.error(err);
        return res.status(400).send('invalid credential');
    }

    let payload;
    let user;
    try {

        const keyModel = await getPublicKey(kid, coinDb);

        const key = keyModel.key;
        const importedKey = await jose.importJWK({
            kty: key.kty,
            e: key.e,
            n: key.n,
            alg: key.alg,
        });

        const decoded = await jose.jwtVerify(cre, importedKey);
        payload = decoded.payload;
    } catch(err) {
        return res.status(400).send(err);
    }


    /**
     * use zod
     * @todo move to schema file
     */
    const gPayload = GPayloadSchema.parse(payload);


    try {
        // user = await prisma.user.findUnique({
        //     where: {sub: gPayload.sub}
        // });

        user = await coinDb.collection('user').findOne(
            {sub: gPayload.sub}
        )

        if (!user) {
            const userData = {
                sub: gPayload.sub,
                issuer: "google",
                name: gPayload.name,
                pic: gPayload.picture
            }
            const result = await coinDb.collection('user').insertOne(userData);
            user = {_id: result.insertedId, ...userData}
        }
    } catch(err) {
        return res.status(400).send(err);
    }

    /**
     * @todo save secretKey to DB and rotate 
     */
    var secretKey = new Uint8Array([0x62, 0x69, 0x74, 0x63, 0x6f, 0x69, 0x6e]);

    const credential = await new jose.SignJWT({name: user.name, pic:user.pic})
        .setProtectedHeader({ alg: 'HS256', kid: 'btc', typ: "JWT" })
        .setIssuedAt()
        .setIssuer('urn:idp.cointracker.io')
        .setAudience('urn:cointracker.io')
        .setExpirationTime('24h')
        .setSubject(user._id.toString())
        .sign(secretKey);

    return res.status(200).send({
        credential,
        u: { name: user.name, pic: user.pic }
    });
}
