import { NextApiRequest, NextApiResponse } from "next"
import { PrismaClient } from "@prisma/client";
import { auth_pub_key } from "@prisma/client";
import axios from "axios";
import * as jose from 'jose';

import CoinService from "services/CoinService";

const prisma = new PrismaClient({
    log: ['warn', 'error']
});

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    try {
        await prisma.$connect()
    } catch (err) {
        console.log(err);
        return res.status(500).send('Error connect DB');
    }

    if(req.method != 'GET') {
        return res.status(404).json({
            result: 'not implemeted'
        })
    }

    // const query = req.query;

    const tags = req.query['tags'];
    // console.log(req.query);
    if(!tags) {
        return res.status(400).json({
            result: 'param coin ids missing'
        })
    }
    // const coins = tags.split(',');

    const topCoinList = await CoinService.getTopCoins(prisma);




    return res.status(200).json(topCoinList);
}