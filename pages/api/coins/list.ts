import { NextApiRequest, NextApiResponse } from "next"
import { PrismaClient } from "@prisma/client";
import CoinService from "services/CoinService";

const prisma = new PrismaClient({
    log: ['warn', 'error']
});

export async function getCoinList() {
    await prisma.$connect();
    
    const topCoinList = await CoinService.getTopCoins(prisma);
    return topCoinList;
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    if(req.method != 'GET') {
        return res.status(404).json({
            result: 'not implemeted'
        })
    }

    try {
        const topCoinList = getCoinList();
        return res.status(200).json(topCoinList);
    } catch (err) {
        return res.status(500).send('Error connect DB');
    }
}