import { NextApiRequest, NextApiResponse } from "next";

import { Db, ObjectId, ReadPreference } from "mongodb";
import { apiWrapper, RequestCtx } from "helpers/api.wrapper";

import { TransactionSchema } from 'schema/transaction.schema';

const wrappedApiHandlers = apiWrapper({
    get: apiGetHandler,
    post: apiPostHandler
});

export default wrappedApiHandlers;

async function getWallet(userId:ObjectId, coinDb:Db) {

    let wallet = await coinDb.collection('wallet_overview').findOne(
        { ownerId: userId }
    );

    if (!wallet) {
        const walletData = {
            ownerId: new ObjectId(userId),
            name: "default wallet",
            assets: {}
        }
        const { insertedId } = await coinDb.collection('wallet_overview').insertOne(walletData);

        wallet = { "_id": insertedId, ...walletData };
    }

    return wallet;
}

async function apiGetHandler(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {

    const coinDb = ctx.coinDb;
    const userId = ctx.userId;

    if(!coinDb || !userId) {
        throw new Error('userId and db connection are required');
    }

    const wallet = await getWallet(userId, coinDb);

    const query = req.query;
    const coinId = query.id;
    const walletId = wallet._id;

    let transactions;
    try {
        transactions = await coinDb.collection('wallet_transaction').find(
            { walletId, coinId },
        ).sort({ createdAt: -1 }
        ).toArray();
    } catch (err) {
        console.error(err);
        return res.status(500).send("database error");
    }

    return res.status(200).json({
        wallet,
        transactions
    });
}

async function apiPostHandler(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {
    const client = ctx.dbClient;
    const coinDb = ctx.coinDb;
    const userId = ctx.userId;

    if (!client || !coinDb || !userId) {
        throw new Error('userId and db connection are required');
    }

    const wallet = await getWallet(userId, coinDb);

    const body = req.body;
    const coinId = body.coinId;
    const walletId = wallet._id;

    let newTransactionData;
    try {
        newTransactionData = TransactionSchema.parse({
            walletId,
            coinId,
            direction: 'BUY',
            amount: Number.parseFloat(body.amount),
            price: Number.parseFloat(body.price),
            cost: Number.parseFloat(body.cost),
            createdAt: new Date()
        });
    } catch(err) {
        console.error(err);
        return res.status(500).send("transaction data format error");
    }

    let session;
    session = client.startSession();

    let tranResult;

    try {
        session.startTransaction({
            readPreference: ReadPreference.primary,
            readConcern: { level: "local" },
            writeConcern: { w: 'majority' },
            maxCommitTimeMS: 1000
        });

        tranResult = await coinDb.collection('wallet_transaction').insertOne(newTransactionData, { session });

        const amountKey = `assets.${coinId}.amount`;
        const costKey = `assets.${coinId}.cost`;
        await coinDb.collection('wallet_overview').findOneAndUpdate(
            { '_id': new ObjectId(walletId) },
            {
                $inc: {
                    [amountKey]: newTransactionData.amount,
                    [costKey]: newTransactionData.cost
                }
            },
            { session }
        )

        await session.commitTransaction();
    } catch (err) {
        await session.abortTransaction();
        console.error("-----transaction error!!------");
        return res.status(401).json({
            result: "database error " + err
        });
    } finally {
        await session.endSession();
    }

    return res.json(tranResult);
}