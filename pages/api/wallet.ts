import { NextApiRequest, NextApiResponse } from "next";
import { apiWrapper, RequestCtx } from "helpers/api.wrapper";

const wrappedHandlers = apiWrapper({
    get: getHandler
})

export default wrappedHandlers;

async function getHandler(req: NextApiRequest, res: NextApiResponse, ctx: RequestCtx) {
    const coinDb = ctx.coinDb;
    const userId = ctx.userId;

    if(!coinDb || !userId) {
        throw new Error('db connection and user are required');
    }

    let wallet;
    try {
        wallet = await coinDb.collection('wallet_overview').findOne({
            ownerId: userId
        });
    } catch(err) {
        console.error(err);
        return res.status(500).send('data base error')
    }

    if(!wallet) {
        wallet = {}
    }

    return res.status(200).json(wallet);
}