
import Head from 'next/head';
import { AppProps } from 'next/app';
import { ComponentProps, FC, ReactElement } from 'react';
import { createTheme, ThemeProvider } from '@mui/material';

import {
    QueryClient,
    QueryClientProvider
} from '@tanstack/react-query';

import Layout from '../components/layout';
import RouteGuard from '../components/RouteGuard';

import '../styles/reset.css'

// interface FCWithLayout extends FC {
//     getLayout: Function
// }

const theme = createTheme({
    palette: {
        primary: {
            main: '#75E6DA',
            light: '#D4F1F4',
            dark: '#189AB4'
        }
    }
});

const queryClient = new QueryClient();

export default function App({ Component, pageProps }: AppProps) {

    const getLayout = (Component as any).getLayout
        || ((page: ReactElement) => {
            return (
            <RouteGuard>
                <Layout>{page}</Layout>
            </RouteGuard>
            )
        })

    return (
        <ThemeProvider theme={theme}>
            <Head>
                <title>Coin Tracker: personal app to track your crypto performance</title>
            </Head>
            <QueryClientProvider client={queryClient}>
                {getLayout(<Component {...pageProps} />)}
            </QueryClientProvider>
        </ThemeProvider>
    );
}
