import Router from 'next/router';
import { useEffect, useState, FC, ReactElement } from "react";

import { Box, LinearProgress, Paper } from "@mui/material";
import axios from 'axios';

/* global google */
declare var google: any
const GOOGLE_CLIENT_URI = "https://accounts.google.com/gsi/client"
const GOOGLE_CLIENT_ID = process.env.NEXT_PUBLIC_GOOL_CLIENT_ID;

export default function Login() {
    const [isLoading, setIsLoading] = useState(false);

    const handleGoogleResponse = async(res:any) => {
        setIsLoading(true);
        let authResult; 
        try {
            authResult = await axios.post(
                '/api/remote-login',
                { credential: res.credential }
            )
        } catch(err) {
            console.error('error connect to IDP server');
            return;
        }
    
        console.log(authResult);

        await localStorage.setItem('credential', authResult.data.credential);
        await localStorage.setItem('u', JSON.stringify(authResult.data.u));
    
        Router.push('/');
        return;
    }
    
    const initButton = () => {
        /** client_id is OK to share with public */
        google.accounts.id.initialize({
            client_id: GOOGLE_CLIENT_ID,
            auto_select: true,
            callback: handleGoogleResponse
        });
    
        google.accounts.id.renderButton(
            document.getElementById('google-login-button'),
            { theme: "", size: "large" }
        );
    }

    useEffect(() => {
        const scriptTag = document.createElement('script');
        scriptTag.src = GOOGLE_CLIENT_URI;
        scriptTag.defer = true;
        scriptTag.async = true;
        scriptTag.addEventListener('load', initButton)
        document.body.appendChild(scriptTag);
        return () => {
            scriptTag.remove();
        }
    }, []);

    return (
        <>
            <Box sx={{padding: "50px 0"}}>
                <h1 style={{textAlign:'center'}}>Log in with Google</h1>
                <div id="google-login-button" style={{width: "240px",margin:"50px auto 15px auto"}}></div>
            </Box>
            { isLoading ? <LinearProgress /> : null }
        </>
    )
}

Login.getLayout = function getLayout(page:ReactElement) {
    return (
        <Paper elevation={3} sx={{margin:"150px auto",maxWidth:"480px"}}>
            {page}
        </Paper>
    )
}