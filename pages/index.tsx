import Router from 'next/router';
import { useState } from 'react';
import { Avatar, Box, Button, IconButton, Modal, Typography } from '@mui/material';
import { List, ListItem, ListItemAvatar, ListItemText } from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert';

import { getCoinList } from './api/coins/list';
import { useWalletQuery, T_Assets } from 'hooks/wallet.hook';
import { local } from 'utils/format';
import { useCoinQuery } from 'hooks/coin.hook';

import CoinSelector from '../components/Coin/Selector';
import Performance from '../components/Performance/Performance';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    overflow: 'auto',
    height: 480
};

type T_Asset_Model = {
    key: string,
    amount: number,
    cost: number,
    currentPrice: number,
    pic: string,
    name: string,
    priceChange24h: number
}

export default function Home({ coins }: { coins:any }) {

    const [modalOpen, setModalOpen] = useState(false);

    function handleOpen() {
        setModalOpen(true);
    }

    function handleClose() {
        setModalOpen(false);
    }

    const _onSelectCoin = (coinId:string) => {
        Router.push(`/asset/${coinId}`);
    }

    const { data, isLoading } = useWalletQuery();
    
    let totalCost = 0, currentValuation = 0, assets: T_Assets, coinIdsStr;

    if(!isLoading && data?.assets) {
        const coinIds = Object.keys(data.assets);
        coinIdsStr = coinIds.join(',');
    }

    const { data: coinData, isLoading: isCoinLoading } = useCoinQuery(coinIdsStr);

    let assetModels: T_Asset_Model[] = [];
    if(!isCoinLoading && data?.assets) {
        Object.entries(data.assets).forEach(([key, asset]) => {
            const coin = coinData.find((c:any) => c.id == key);
            const assetModel:T_Asset_Model = {
                key,
                name: coin.name,
                pic: coin.image,
                amount: asset.amount,
                cost: asset.cost,
                currentPrice: coin.current_price,
                priceChange24h: coin.price_change_24h < 0.01 ? coin.price_change_24h.toFixed(4) : coin.price_change_24h.toFixed(2)
            };
            assetModels.push(assetModel);
            
            totalCost += asset.cost;
            currentValuation += assetModel.amount * assetModel.currentPrice;
        })
    }
    // console.log(assetModels);

    return (
        <>
            <Performance cost={totalCost} valuation={currentValuation}/>
            {assetModels.length > 0 ?
            <Box pl={4} pr={4}>
                <Box sx={{display:'flex',justifyContent:'space-between'}}>
                    <Typography variant="h5" component="div">
                        Coin Assets
                    </Typography>
                    <Button variant="outlined" onClick={handleOpen} disabled={false}>+</Button>
                </Box>
                <List sx={{bgcolor: 'background.paper'}}>
                    {assetModels.map((asset) => (
                        <ListItem key={asset.key} sx={{
                            padding:'10px 25px',
                            backgroundColor:'#f7f7f7',
                            borderRadius:'4px',
                            border:'1px solid white',
                            margin:'0 0 10px 0'}}
                            secondaryAction={
                                <IconButton onClick={() => _onSelectCoin(asset.key)}>
                                    <MoreVertIcon></MoreVertIcon>
                                </IconButton>
                            }>
                            <ListItemAvatar sx={{minWidth:32}}>
                                <Avatar sx={{width:24,height:24}} alt={asset.name} src={asset.pic} />
                            </ListItemAvatar>
                            <ListItemText sx={{width:180,flex:'none'}} primary={asset.name} secondary={asset.amount.toFixed(6)} />
                            <ListItemText
                                sx={{width: 180,flex:'none'}}
                                primary={`$${asset.currentPrice}`}
                                secondary={`${asset.priceChange24h} (24h)`}
                            />
                            <ListItemText
                                sx={{textAlign:'right',paddingRight:'50px'}}
                                primary={`USD ${local(asset.amount * asset.currentPrice)}`}
                            />
                            
                        </ListItem>
                    ))}
                </List>
            </Box>
            :
            <Box sx={{ textAlign: 'center' }} p={4}>
                <Typography variant="h4" component="div">
                    You have no transactions
                </Typography>
                <Typography component="div">
                    Please add transactions to start tracking your crypto holdings.
                </Typography>
                <Button sx={{ marginTop: 4 }} variant="outlined" onClick={handleOpen} disabled={false}>ADD TRANSACTION</Button>
            </Box>
            }

            <Modal
                open={modalOpen}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        Select a coin
                    </Typography>
                    <CoinSelector coins={coins} onSelect={_onSelectCoin} />
                </Box>
            </Modal>
        </>
    )
}

export async function getServerSideProps() {
    try {
        const data = await getCoinList();
        return {
            props: { coins: data.coins }
        }
    } catch (err) {
        console.log(err);
    }
}