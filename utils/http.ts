import axios from 'axios';

let token;
if (typeof window !== 'undefined') {
    token = localStorage.getItem('credential');
}

const http = axios.create({
    headers: {
        'Authorization': 'Bearer ' + token
    }
})

http.interceptors.response.use(
    response => response,
    error => {
        // auth token error
        if(error.response.status == 419) {

            console.error('auth response code 419!!!');

            localStorage.clear();
            window.location.href = '/login'
        }
    }
)

export default http;