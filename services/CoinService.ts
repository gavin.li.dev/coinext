import { CoinItem, PrismaClient, top_coin_list } from "@prisma/client"; 
import axios from "axios";

const topCoinURI = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=20&page=1&sparkline=false";

const CoinService: any = {};

export type CoinData = {
    id: String
    name: String
    image: String
    current_price: Number
    price_change_percentage_24h: Number
}

CoinService.getTopCoins = async ($db: PrismaClient): Promise<top_coin_list> => {
    let topCoinList = await $db.top_coin_list.findFirst({
        orderBy: {createdAt: 'desc'}
    });

    
    if(topCoinList && topCoinList.createdAt) {
        const timeDiff = Date.now() - topCoinList.createdAt.getTime();
        if(Math.floor( timeDiff / (3600 * 1000) ) < 24) { 
            return topCoinList;
        }
    }

    const remoteRequest = await axios.get(topCoinURI);

    if( !remoteRequest || !Array.isArray(remoteRequest.data) ) {
        throw new Error('coingecko API error');
    }

    let coinData:Array<any> = [];
    remoteRequest.data.map((d: CoinItem) => {
        coinData.push({
            id: d.id,
            symbol: d.symbol,
            name: d.name,
            image: d.image
        })
    }) 
    
    topCoinList = await $db.top_coin_list.create({
        data: {
            coins: coinData
        }
    })
    return topCoinList;
}

CoinService.getCoin = async (coinId: string) => {
    const getAPIEndpoin = (id:string) => {
        return `https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=${id}&order=market_cap_desc&per_page=1&page=1&sparkline=false`
    }

    const apiEndpoint = getAPIEndpoin(coinId);
    const remoteRequest = await axios.get(apiEndpoint);

    if( !remoteRequest ) {
        throw new Error('coingecko API error');
    }

    const coinData: CoinData = remoteRequest.data[0];
    return coinData;
}

export default CoinService;