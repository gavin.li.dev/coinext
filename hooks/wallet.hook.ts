import { useQuery } from '@tanstack/react-query';
import http from 'utils/http';

type T_Asset = {
    amount: number,
    cost: number
}
export type T_Assets = {
    [key: string]: T_Asset
}
export type T_Wallet = {
    name: string,
    assets: T_Assets
}

async function getWallets(): Promise<T_Wallet> {
    const { data } = await http.get('/api/wallet');
    return data;
}

export function useWalletQuery() {
    return useQuery(
        ["wallet_overview"],
        getWallets
    );
}