import axios from 'axios';
import { useQuery } from '@tanstack/react-query';

async function getCoin(coinIdsStr: string | undefined) {
    const {data} = await axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=${coinIdsStr}&order=market_cap_desc&per_page=1&page=1&sparkline=false`);
    return data;
}

export function useCoinQuery(coinIdsStr: string | undefined) {
    return useQuery(
        ['coin', coinIdsStr],
        () => getCoin(coinIdsStr),
        {
            select: (dataArr) => {
                if(dataArr.length == 0) {
                    throw new Error('remote API error');
                }
                return dataArr;
            },
            enabled: !!coinIdsStr
        }
    );
}