import { useQuery, useQueryClient, useMutation } from '@tanstack/react-query';
import http from 'utils/http';

type TA = {
    direction: 'BUY'
    amount: Number
    unitPrice: Number
    cost: Number
    createdAt: Date,
    id?: String
}

async function getTransactions(coinId: string | undefined) {
    const { data: transactionData } = await http.get('/api/transaction?id=' + coinId);
    return transactionData;
}

async function postTransaction(data:any) {
    return await http.post('/api/transaction', data);
}

export function useTransactionQuery(coinId: string | undefined) {
    return useQuery(
        ['transactions', coinId],
        () => getTransactions(coinId),
        {
            select: (data) => {
                const tranArray = data.transactions.map((d:any) => {
                    const newVal:TA = {
                        id: d._id,
                        direction: d.direction,
                        amount: d.amount,
                        unitPrice: d.price,
                        cost: d.cost,
                        createdAt: d.createdAt
                    }
                    return newVal;
                })
                const newVal = {...data, transactions: tranArray}

                return newVal;
            },
            enabled: !!coinId
        }
    )
}

export function useTransactionMutation() {
    const queryClient = useQueryClient();

    const mutation = useMutation(postTransaction, {
        
        onMutate: (data) => {
            const optimisticData = {...data, _id: Math.random()*1000} 
            const oldQueryData: any = queryClient.getQueryData(['transactions', data.coinId]);
            

            const newQueryData = {
                ...oldQueryData,
                transactions: [optimisticData, ...oldQueryData.transactions]
            }
            queryClient.setQueriesData(['transactions', data.coinId], newQueryData);

            return {queryKey: ['transactions', data.coinId]}
        },
        onSuccess: (_data, _variables, con) => {
            if(!con) {
                throw new Error('query keys are required')
            }
            queryClient.invalidateQueries(con.queryKey);
        }
    });

    return mutation;
}